# PhotoRenamer, C# .NET auto-photo renaming app
This console app will rename .JPG photos with a yyyymmdd_hhmmss.jpg format based on the EXIF "Date Taken" field.