﻿using System;
using System.Text;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace PhotoRenamer
{
    /// <summary>
    /// Lets the calling thread know when a file rename is complete
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void OnFileCompleteHandler(object sender, EventArgs e);

    /// <summary>
    /// Class with methods to automatically rename JPG files based on the EXIF date taken data
    /// </summary>
    class FileRenamer
    {
        // Callback for when a renaming function is complete
        public event OnCompleteHandler OnFileComplete;

        int offsetSeconds;
        bool addYearMonthFolder;

        public FileRenamer(bool addYearMonthFolder, int offsetSeconds = 0)
        {
            this.offsetSeconds = offsetSeconds;
            this.addYearMonthFolder = addYearMonthFolder;
        }

        /// <summary>
        /// The main file renaming function
        /// </summary>
        /// <param name="inputfile">The file to rename</param>
        public void RenameFile(object inputfile)
        {
            string filename = (string)inputfile;
            string newfilename = "";
            string fileExtension = "";
            DateTime picdate = new DateTime();

            picdate = File.GetLastWriteTime(filename);
            fileExtension = Path.GetExtension(filename);
            picdate = picdate.AddSeconds(offsetSeconds);

            newfilename = picdate.ToString("yyyyMMdd-HHmmss") + fileExtension.ToLower();

            if (addYearMonthFolder)
            {
                CreateDirectories(picdate);
                newfilename = System.Environment.CurrentDirectory + "\\" + picdate.ToString("yyyy") + "\\" + picdate.ToString("MM") + "\\" + newfilename;
            }
            TryMove(filename, newfilename, 0);
        }

        void CreateDirectories(DateTime date)
        {
            Directory.CreateDirectory(System.Environment.CurrentDirectory + "\\" + date.ToString("yyyy"));
            Directory.CreateDirectory(System.Environment.CurrentDirectory + "\\" + date.ToString("yyyy") + "\\" + date.ToString("MM"));
        }

        /// <summary>
        /// Recursive renaming function.  If file already exists it will re-call with an incremented filename.
        /// </summary>
        /// <param name="oldfilename">Current file name</param>
        /// <param name="newfilename">New file name</param>
        /// <param name="inc">Increment through the recursion</param>
        void TryMove(string oldfilename, string newfilename, int inc)
        {
            string extension = Path.GetExtension(oldfilename);

            if (inc > 100)
            {
                Console.WriteLine("Overflow on {0}.  Too many identical photo date times!", oldfilename);
                if (OnFileComplete != null)
                    OnFileComplete(this, EventArgs.Empty);
                return;
            }

            string destfilename = newfilename;

            if (inc > 0)
                destfilename = newfilename.Replace(extension, " (" + inc.ToString() + ")" + extension.ToLower());

            try
            {
                File.Move(oldfilename, destfilename);
            }
            catch (IOException)
            {
                TryMove(oldfilename, newfilename, inc+1);
                return;
            }

            Console.WriteLine("{0} ==> {1}", oldfilename, destfilename);
            if(OnFileComplete != null)
                OnFileComplete(this, EventArgs.Empty);
        }
    }
}
