using System;
using System.IO;
using System.Threading;

namespace PhotoRenamer
{
    /// <summary>
    /// Main class to run the renamer object
    /// </summary>
    public class Rename
    {
        // 25 threads seemed to work fine on my Core i7 920
        const int MAX_THREADS = 10;
        public int threadcount;
        JpgRenamer renamer;
        FileRenamer fileRenamer;

        public Rename(bool addYearMonthFolder, int offset)
        {
            threadcount = 0;
            renamer = new JpgRenamer(addYearMonthFolder, offset);
            renamer.OnComplete += new OnCompleteHandler(renamer_OnComplete);
            fileRenamer = new FileRenamer(addYearMonthFolder, offset);
        }

        void renamer_OnComplete(object sender, EventArgs e)
        {
            threadcount--;
        }

        public void RenameFiles()
        {
            foreach (string file in Directory.GetFiles(@".", "*.jpg"))
            {
                // Did this to make sure we didn't open up a ton of threads
                // while (threadcount >= MAX_THREADS)
                //    Thread.Sleep(50);

                // Thread t = new Thread(new ParameterizedThreadStart(renamer.RenameFile));
                // t.Start(file);
                renamer.RenameFile(file);
                // threadcount++;
            }
            foreach (string file in Directory.GetFiles(@".", "*.mov"))
            {
                fileRenamer.RenameFile(file);
            }
            foreach (string file in Directory.GetFiles(@".", "*.mpg"))
            {
                fileRenamer.RenameFile(file);
            }
            foreach (string file in Directory.GetFiles(@".", "*.avi"))
            {
                fileRenamer.RenameFile(file);
            }
            foreach (string file in Directory.GetFiles(@".", "*.mp4"))
            {
                fileRenamer.RenameFile(file);
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            int offset = 0;
            bool addYearMonthFolder = false;

            if (args.Length > 0)
            {
                offset = Convert.ToInt32(args[0]);
                if (args.Length == 2)
                    addYearMonthFolder = true;
            }

            Rename r = new Rename(addYearMonthFolder, offset);
            r.RenameFiles();
        }
    }
}
