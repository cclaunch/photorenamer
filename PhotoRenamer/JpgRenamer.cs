﻿using System;
using System.Text;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace PhotoRenamer
{
    /// <summary>
    /// Lets the calling thread know when a file rename is complete
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void OnCompleteHandler(object sender, EventArgs e);

    /// <summary>
    /// Class with methods to automatically rename JPG files based on the EXIF date taken data
    /// </summary>
    class JpgRenamer
    {
        // Callback for when a renaming function is complete
        public event OnCompleteHandler OnComplete;

        // EXIF header offset for the date taken field
        const int DATE_TAKEN = 36867;
        int offsetSeconds;
        bool addYearMonthFolder;

        public JpgRenamer(bool addYearMonthFolder, int offsetSeconds = 0)
        {
            this.offsetSeconds = offsetSeconds;
            this.addYearMonthFolder = addYearMonthFolder;
        }

        /// <summary>
        /// The main file renaming function
        /// </summary>
        /// <param name="inputfile">The file to rename</param>
        public void RenameFile(object inputfile)
        {
            string filename = (string)inputfile;
            string newfilename = "";
            string filedatetime = "";
            DateTime picdate = new DateTime();
            Image img = null;
            PropertyItem prop = null;
            bool propFailed = false;

            try
            {
                img = Image.FromFile(filename);
                prop = img.GetPropertyItem(DATE_TAKEN);
            }
            catch
            {
                // Console.WriteLine("Failed to get property on {0}", filename);
                propFailed = true;
            }

            if (!propFailed)
            {
                filedatetime = Encoding.ASCII.GetString(prop.Value);

                if (filedatetime.Length < 10)
                {
                    // Console.WriteLine("Bad date from {0}", filename);
                    picdate = File.GetLastWriteTime(filename);
                }
                else
                {
                    filedatetime = filedatetime.Substring(0, 10).Replace(':', '-') + filedatetime.Substring(10, filedatetime.Length - 10);
                    if (!DateTime.TryParse(filedatetime, out picdate))
                    {
                        // Console.WriteLine("Failed to get date from {0}", filename);
                        picdate = File.GetLastWriteTime(filename);
                    }
                }
            }
            else
                picdate = File.GetLastWriteTime(filename);

            picdate = picdate.AddSeconds(offsetSeconds);
            newfilename = picdate.ToString("yyyyMMdd-HHmmss") + ".jpg";
            if (img != null)
            {
                img.Dispose();
                if (addYearMonthFolder)
                {
                    CreateDirectories(picdate);
                    newfilename = System.Environment.CurrentDirectory + "\\" + picdate.ToString("yyyy") + "\\" + picdate.ToString("MM") + "\\" + newfilename;
                }
                TryMove(filename, newfilename, 0);
            }
            else
            {
                Console.WriteLine("Invalid image file {0}", filename);
            }
        }

        void CreateDirectories(DateTime date)
        {
            Directory.CreateDirectory(System.Environment.CurrentDirectory + "\\" + date.ToString("yyyy"));
            Directory.CreateDirectory(System.Environment.CurrentDirectory + "\\" + date.ToString("yyyy") + "\\" + date.ToString("MM"));
        }

        /// <summary>
        /// Recursive renaming function.  If file already exists it will re-call with an incremented filename.
        /// </summary>
        /// <param name="oldfilename">Current file name</param>
        /// <param name="newfilename">New file name</param>
        /// <param name="inc">Increment through the recursion</param>
        void TryMove(string oldfilename, string newfilename, int inc)
        {
            if (inc > 100)
            {
                Console.WriteLine("Overflow on {0}.  Too many identical photo date times!", oldfilename);
                if (OnComplete != null)
                    OnComplete(this, EventArgs.Empty);
                return;
            }

            string destfilename = newfilename;

            if (inc > 0)
                destfilename = newfilename.Replace(".jpg", " (" + inc.ToString() + ").jpg");

            try
            {
                File.Move(oldfilename, destfilename);
            }
            catch (IOException)
            {
                TryMove(oldfilename, newfilename, inc+1);
                return;
            }

            Console.WriteLine("{0} ==> {1}", oldfilename, destfilename);
            if(OnComplete != null)
                OnComplete(this, EventArgs.Empty);
        }
    }
}
